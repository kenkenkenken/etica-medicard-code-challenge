import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import styles from './InputArrayDoubler.module.css';

export function InputArrayDoubler() {
  //const count = useSelector(selectCount);
  const dispatch = useDispatch();
  const [inputArray, setInputArray] = useState();
  const [inputArrayDoubled, setInputArrayDoubled] = useState();

  //const incrementValue = Number(incrementAmount) || 0;
  const doubleInputArray = (inputArray) => {
      const numArray = inputArray.split(',')
      let newArray = [];
      numArray.forEach(function (num) { newArray.push(num * 2); });
      setInputArrayDoubled(newArray.join(','))
  }

  return (
    <div>
      <div className={styles.row}>
          <div className={styles.column}>
              <h1 className={styles.label}>Input</h1>
              <p className={styles.label}>Array</p>
              <input
                  className={styles.textbox}
                  value={inputArray}
                  onChange={(e) => doubleInputArray(e.target.value)}
              />
          </div>
          <div className={styles.column}>
              <h1 className={styles.label}>Output</h1>
              <p className={styles.label}>Double</p>
              <input
                  className={styles.textbox}
                  value={inputArrayDoubled}
              />
          </div>
      </div>
    </div>
  );
}
